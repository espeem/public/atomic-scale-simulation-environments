#!/bin/sh

TARGETFOLDER=/opt/siesta_build

mkdir -p $TARGETFOLDER

wget https://zlib.net/zlib-1.2.11.tar.gz
tar -xzvf zlib-1.2.11.tar.gz
rm zlib-1.2.11.tar.gz
cd zlib-1.2.11
./configure --prefix=$TARGETFOLDER/zlib
make
make install
cd ..
rm -r zlib-1.2.11

export LD_LIBRARY_PATH=$TARGETFOLDER/zlib/lib\:$LD_LIBRARY_PATH
export INCLUDE=$TARGETFOLDER/zlib/include\:$INCLUDE

######
##  HDF 5
######

wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.1/src/hdf5-1.10.1.tar.gz
tar -xzvf hdf5-1.10.1.tar.gz
rm hdf5-1.10.1.tar.gz
cd hdf5-1.10.1

LDFLAGS="-L$TARGETFOLDER/zlib/lib -Wl,-rpath=$TARGETFOLDER/zlib/lib" \
./configure --prefix=$TARGETFOLDER/hdf5 --enable-cxx --enable-shared --enable-static --enable-fortran --enable-parallel --with-zlib=$TARGETFOLDER/zlib

make
make install
cd ..
rm -r hdf5-1.10.1

export PATH=$TARGETFOLDER/hdf5/bin\:$PATH
export LD_LIBRARY_PATH=$TARGETFOLDER/hdf5/lib\:$LD_LIBRARY_PATH
export INCLUDE=$TARGETFOLDER/hdf5/include\:$INCLUDE

######
##  NetCDF Fortran
######

wget https://github.com/Unidata/netcdf-fortran/archive/v4.4.4.tar.gz --no-check-certificate
tar -xzvf v4.4.4.tar.gz
rm v4.4.4.tar.gz
cd netcdf-fortran-4.4.4

CPPFLAGS="-I$TARGETFOLDER/hdf5/include -I$TARGETFOLDER/zlib/include" \
LDFLAGS="-L$TARGETFOLDER/hdf5/lib -Wl,-rpath=$TARGETFOLDER/hdf5/lib "``"-L$TARGETFOLDER/zlib/lib -Wl,-rpath=$TARGETFOLDER/zlib/lib" \
./configure --prefix=$TARGETFOLDER/netcdf-fortran --enable-shared --enable-static

make
make install
cd ..
rm -r netcdf-fortran-4.4.4

export PATH=$TARGETFOLDER/netcdf-fortran/bin\:$PATH
export LD_LIBRARY_PATH=$TARGETFOLDER/netcdf-fortran/lib\:$LD_LIBRARY_PATH
export INCLUDE=$TARGETFOLDER/netcdf-fortran/include\:$INCLUDE
