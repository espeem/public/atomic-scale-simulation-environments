# Atomic-scale Simulation Environments

This repo is dedicated to setting up simulation environments for atomic-scale simulation software. These types of software often contain complex dependencies and are difficult to build correctly.  Hence this repo sharing best practices.